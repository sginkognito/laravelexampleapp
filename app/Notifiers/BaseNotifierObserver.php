<?php

namespace App\Notifiers;

use App\Services\ObservableService;
use SplObserver;

abstract class BaseNotifierObserver implements SplObserver {
    private $observable;
    private $index;

    function __construct(ObservableService $observable)
    {
        static $sindex=0;

        $this->index=$sindex++;

        $this->observable = $observable;
        $observable->attach($this);
    }
}