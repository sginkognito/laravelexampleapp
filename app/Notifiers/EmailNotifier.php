<?php

namespace App\Notifiers;

use App\Mail\NotifyProductChange;
use Illuminate\Support\Facades\Mail;

class EmailNotifier extends BaseNotifierObserver {

    public function update($subject)
    {
        $email = env('EMAIL_TO_NOTIFY', 'test@test.com');
        Mail::to($email)->send(new NotifyProductChange($subject));
    }

}