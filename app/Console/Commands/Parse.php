<?php

namespace App\Console\Commands;

use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:parse {dir}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $categoryService = new CategoryService();
        $productService = new ProductService();
        $dir = $this->argument('dir');
        // $categories = json_decode(file_get_contents($dir . '/categories.json'), true);

        // foreach ($categories as $category) {
        //     $categoryService->save($category);
        // }

        $products = json_decode(file_get_contents($dir . '/products.json'), true);
        foreach ($products as $product) {
            $productService->save($product);
        }
        return $products;
    }
}
