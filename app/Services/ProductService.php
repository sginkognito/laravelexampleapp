<?php

namespace App\Services;

use App\Models\Product;
use App\Notifiers\EmailNotifier;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class ProductService extends ObservableService {
    public $productModel;

    public function __construct() {
        parent::__construct();
        new EmailNotifier($this);
    }

    public function save(array $product) {
        $validator = Validator::make($product, [
            'id' => 'exists:products,id',
            'title' => 'required|string|min:3|max:12',
            'price' => 'required|numeric|min:0|max:200',
            'eId' => 'integer',
            'categoryEId' => 'array',
            'categoryEId.*' => 'exists:categories,eId',
            'categoriesEId' => 'array',
            'categoriesEId.*' => 'exists:categories,eId'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        if(isset($product['id'])) {
            $this->productModel = Product::find($product['id']);
        } else if(isset($product['eId'])) {
            $this->productModel = $this->getModelByEId($product['eId']);
        } else {
            $this->productModel = new Product();
        }

        $this->productModel->title = $product['title'];
        $this->productModel->price = $product['price'];
        $this->productModel->save();

        if(isset($product['categoryEId'])) {
            $this->productModel->categories()->sync($product['categoryEId']);
        } else if (isset($product['categoriesEId'])) {
            $this->productModel->categories()->sync($product['categoriesEId']);
        }

        $this->notify();

        return $this->productModel;
    }

    public function delete(int $id) {
        $product = Product::find($id);
        if (!$product) {
            throw new InvalidArgumentException('Product with such id not found');
        }

        $product->delete();
    }

    private function getModelByEId($eId) {
        $model = Product::where('eId', $eId)->first();
        
        if(!$model) {
            $model = new Product();
            $model->eId = $eId;
        }

        return $model;
    }
}