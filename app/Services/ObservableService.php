<?php

namespace App\Services;

use SplObjectStorage;
use SplObserver;
use SplSubject;

abstract class ObservableService implements SplSubject {
    private $storage;

    function __construct()
    {
        $this->storage = new SplObjectStorage();
    }

    function attach(SplObserver $observer)
    {
        $this->storage->attach($observer);
    }

    function detach(SplObserver $observer)
    {
        $this->storage->detach($observer);
    }

    function notify()
    {
        foreach($this->storage as $obj)
        {
            $obj->update($this);
        }
    }
}