<?php

namespace App\Services;

use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class CategoryService {
    public function save(array $category) {
        $validator = Validator::make($category, [
            'title' => 'required|min:3|max:12',
            'eId' => 'required|integer'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        $categoryModel = Category::where('eId', $category['eId'])->first();
        if(!$categoryModel) {
            $categoryModel = new Category();
            $categoryModel->eId = $category['eId'];
        }

        $categoryModel->title = $category['title'];
        $categoryModel->save();

        return $categoryModel;
    }
}