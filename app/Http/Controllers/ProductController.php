<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index() 
    {
        return Product::all();
    }

    public function store(Request $request)
    {
        $productService = new ProductService();
        
        try {
            return $productService->save($request->all());
        } catch (Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function show(Product $product)
    {
        return $product;
    }

    public function update(Request $request, $id)
    {
        $productService = new ProductService();
        
        try {
            return $productService->save($request->all());
        } catch (Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function destroy(Request $request, $id)
    {
        $productService = new ProductService();
        
        try {
            return $productService->delete($id);
        } catch (Exception $e) {
            return abort(500, $e->getMessage());
        }
    }
}
